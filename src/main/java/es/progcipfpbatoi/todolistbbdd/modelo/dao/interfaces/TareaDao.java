package es.progcipfpbatoi.todolistbbdd.modelo.dao.interfaces;

import java.time.LocalDate;
import java.util.ArrayList;

import es.progcipfpbatoi.todolistbbdd.excepciones.AlreadyExistsException;
import es.progcipfpbatoi.todolistbbdd.excepciones.NotFoundException;
import es.progcipfpbatoi.todolistbbdd.modelo.entidades.Tarea;

public interface TareaDao {

	/**
     * Devuelve todas las tareas
     * @return
     */
    ArrayList<Tarea> findAll();
    
    /**
     * Devuelve las tareas cuyo atributo realizado coincide con @isRealizada y la fecha de vencimiento coincide @vencimiento
     * Si alguno de los atributos es null no deberá tenerse en cuenta
     *
     * @param isRealizada
     * @param vencimiento
     * @param usuario
     *
     * @return
     */
    ArrayList<Tarea> findAllWithParams(Boolean isRealizada, LocalDate fecha, String usuario);
    

    /**
     * Devuelve todas las tareas cuyo usuario coincide con @usuario
     *
     * @param usuario
     * @return
     */
    ArrayList<Tarea> findAllWithParams(String usuario);

    /**
     * Devuelve la tarea cuyo codigo identificativo coincide con @id
     *
     * @param id
     * @return null, si la  tarea con @id no existe
     */
    Tarea findById(int id);

    /**
     * Devuelve la tarea cuyo codigo identificativo coincide con @id o lanza una excepción si no encuentra
     * una tarea con @id
     *
     * @param id
     * @return
     */
    Tarea getById(int id) throws NotFoundException;

    /**
     * Inserta una nueva tarea en la base de datos si ésta no existe ya en la base de datos
     *
     * @param tarea
     */
    void add(Tarea tarea) throws AlreadyExistsException;
    
    /**
     * Actualiza la @tarea, si existe, para que contenga los datos recibidos en @tarea
     * 
     * @param tarea  
     */
    void update(Tarea tarea) throws NotFoundException;
    
    /**
     *  Elmina la tarea si existe 
     *  
     * @param codigo
     */
    void delete(Tarea tarea) throws NotFoundException;
}




